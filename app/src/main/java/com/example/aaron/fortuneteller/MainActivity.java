package com.example.aaron.fortuneteller;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public EditText editText;
    public Button button, buttonDone;
    public TextView textView, textView2;
    public MediaPlayer wand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         wand = MediaPlayer.create(this, R.raw.magic_wand);

        textView = (TextView) findViewById(R.id.text_here);
        button = (Button) findViewById(R.id.button_testluck);
        buttonDone = (Button) findViewById(R.id.press_done);
        editText = (EditText) findViewById(R.id.edit_message);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wand.start();
                setFortune();
                editText.setText(" ");
            }
        });
        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    public void setFortune() {
        String userString = editText.getText().toString();
        FortuneTellerApp fortune = new FortuneTellerApp();
        String lowerCase;
        lowerCase = userString.toLowerCase();

        if (lowerCase.contains("how")) {
            textView.setText(fortune.getHowResponses());
        } else if (lowerCase.contains("who")) {
            textView.setText(fortune.getWhoResponses());
        } else if (lowerCase.contains("when")) {
            textView.setText(fortune.getWhenResponses());
        } else if (lowerCase.contains("what")) {
            textView.setText(fortune.getWhatResponses());
        } else if (lowerCase.contains("where")) {
            textView.setText(fortune.getWhereResponses());
        } else if (lowerCase.contains("am")){
            textView.setText(fortune.getAmResponses());
        } else {
            textView.setText(R.string.real_question);
        }
    }
}
